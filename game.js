//Burak Yalcin
//brkylcn12@gmail.com
//
//
//2020
//Pong Game for Insider

(function () {
  var CSS = {
    arena: {
      width: 900,
      height: 600,
      background: "#62247B",
      position: "fixed",
      top: "50%",
      left: "50%",
      zIndex: "999",
      transform: "translate(-50%, -50%)",
    },
    ball: {
      width: 15,
      height: 15,
      position: "absolute",
      top: 0,
      left: 0,
      borderRadius: 50,
      background: "#C6A62F",
    },
    line: {
      width: 0,
      height: 600,
      borderLeft: "2px dashed #C6A62F",
      position: "absolute",
      top: 0,
      left: "50%",
    },
    stick: {
      width: 12,
      height: 85,
      position: "absolute",
      background: "#C6A62F",
      transition: "0.1s",
    },
    stick1: {
      left: 0,
      top: 100,
    },
    stick2: {
      right: 0,
      top: 100,
    },
    label: {
      top: 20,
      color: "#C6A62F",
      fontSize: 40,
      position: "absolute",
      fontFamily: "sans-serif",
    },
    score1: {
      left: 225,
    },
    score2: {
      right: 225,
    },
    messageLabel: {
      marginLeft: "auto",
      marginRight: "auto",
      marginTop: 250,
      left: 0,
      right: 0,
      textAlign: "center",
    },
  };

  var CONSTS = {
    gameSpeed: 20,
    score1: localStorage.getItem("score1"),
    score2: localStorage.getItem("score2"),
    stick1Speed: 0,
    stick2Speed: 0,
    ballTopSpeed: 0,
    ballLeftSpeed: 0,
  };

  function start() {
    draw();
    setEvents();
    roll();
    loop();
  }

  function draw() {
    $("<div/>", { id: "pong-game" }).css(CSS.arena).appendTo("body");
    $("<div/>", { id: "pong-line" }).css(CSS.line).appendTo("#pong-game");
    $("<div/>", { id: "pong-ball" }).css(CSS.ball).appendTo("#pong-game");
    $("<div/>", { id: "stick-1" })
      .css($.extend(CSS.stick1, CSS.stick))
      .appendTo("#pong-game");

    $("<div/>", { id: "stick-2" })
      .css($.extend(CSS.stick2, CSS.stick))
      .appendTo("#pong-game");

    $("<div/>", { id: "score-1" })
      .css($.extend(CSS.score1, CSS.label))
      .appendTo("#pong-game");

    $("<div/>", { id: "score-2" })
      .css($.extend(CSS.score2, CSS.label))
      .appendTo("#pong-game");
  }

  function setEvents() {
    //Keydown Events
    $(document).on("keydown", function (e) {
      if (e.keyCode == 87) {
        CONSTS.stick1Speed = -20;
      }
      if (e.keyCode == 38) {
        CONSTS.stick2Speed = -20;
      }

      if (e.keyCode == 83) {
        CONSTS.stick1Speed = 20;
      }

      if (e.keyCode == 40) {
        CONSTS.stick2Speed = 20;
      }
    });

    //Keyup Events
    $(document).on("keyup", function (e) {
      if (e.keyCode == 87) {
        CONSTS.stick1Speed = 0;
      }

      if (e.keyCode == 38) {
        CONSTS.stick2Speed = 0;
      }

      if (e.keyCode == 83) {
        CONSTS.stick1Speed = 0;
      }
      if (e.keyCode == 40) {
        CONSTS.stick2Speed = 0;
      }
    });
  }

  function roll() {
    CSS.ball.top = 300;
    CSS.ball.left = 450;

    var side = -1;

    //Randomization of the game.

    if (Math.random() < 0.5) {
      side = 1;
    }

    CONSTS.ballTopSpeed = -10;
    CONSTS.ballLeftSpeed = side * 10;
  }

  function loop() {
    window.pongLoop = setInterval(function () {
      //Updating Sticks' Positions CSS and Values
      CSS.stick1.top += CONSTS.stick1Speed;
      $("#stick-1").css("top", CSS.stick1.top);

      CSS.stick2.top += CONSTS.stick2Speed;
      $("#stick-2").css("top", CSS.stick2.top);

      //Updating Ball Position Values

      CSS.ball.top += CONSTS.ballTopSpeed;
      CSS.ball.left += CONSTS.ballLeftSpeed;

      //Redirecting the ball (mirroring) from the top and bottom borders of the arena.

      if (
        CSS.ball.top <= 0 ||
        CSS.ball.top >= CSS.arena.height - CSS.ball.height
      ) {
        CONSTS.ballTopSpeed = CONSTS.ballTopSpeed * -1;
      }

      //Sticks Arena Limitation to avoid overmovement.

      if (CSS.stick1.top < 0) {
        CSS.stick1.top = 0;
      } else if (CSS.stick1.top + CSS.stick.height > CSS.arena.height) {
        CSS.stick1.top = CSS.arena.height - CSS.stick.height;
      }

      if (CSS.stick2.top < 0) {
        CSS.stick2.top = 0;
      } else if (CSS.stick2.top + CSS.stick.height > CSS.arena.height) {
        CSS.stick2.top = CSS.arena.height - CSS.stick.height;
      }

      //Detecting the stick which is scored
      //Detecting the ball, when it touches the opponent's side.

      if (CSS.ball.left <= CSS.stick.width) {
        if (
          CSS.ball.top >= CSS.stick1.top &&
          CSS.ball.top + CSS.ball.height < CSS.stick1.top + CSS.stick.height
        ) {
          CSS.ball.left = CSS.stick.width;
          CONSTS.ballLeftSpeed *= -1;
        } else {
          CONSTS.score2++;
          roll();
        }
      } else if (
        CSS.ball.left + CSS.ball.width >=
        CSS.arena.width - CSS.stick.width
      ) {
        if (
          CSS.ball.top >= CSS.stick2.top &&
          CSS.ball.top + CSS.ball.width < CSS.stick2.top + CSS.stick.height
        ) {
          CSS.ball.left = CSS.arena.width - CSS.stick.width - CSS.ball.width;
          CONSTS.ballLeftSpeed *= -1;
        } else {
          CONSTS.score1++;
          roll();
        }
      }

      //Saving the score values to the localStorage.

      localStorage.setItem("score1", CONSTS.score1);
      localStorage.setItem("score2", CONSTS.score2);

      //Ending the game
      //Showing the winner

      if (CONSTS.score1 == 5 || CONSTS.score2 == 5) {
        clearInterval(window.pongLoop);

        var winner = "LEFT";
        if (CONSTS.score2 == 5) winner = "RIGHT";

        $("#pong-game").children().hide();
        $("<div>" + winner + " side is the winner!<div/>")
          .css($.extend(CSS.messageLabel, CSS.label))
          .appendTo("#pong-game");

        localStorage.setItem("score1", 0);
        localStorage.setItem("score2", 0);
      }

      //Score Table
      $("#score-1").html(CONSTS.score1);
      $("#score-2").html(CONSTS.score2);

      //Updating the Ball's position CSS
      $("#pong-ball").css({ top: CSS.ball.top, left: CSS.ball.left });
    }, CONSTS.gameSpeed);
  }

  start();
})();
